import pygame
import game
def run():
    pygame.init()
    display = pygame.display.set_mode((1920,1080))
    pygame.display.set_caption("The best game")
    main = game.Game(display)
    
    main.runLoop()

run()
quit()