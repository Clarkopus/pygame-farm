import pygame
from entities import entity
from entities.crops import *
from entities.crops import crop
class Game:

    def __init__(self,display):
        self.display = display
        self.entityList = []
        self.populateWord()
        self.running = True
        self.clock = pygame.time.Clock()
        self.player = self.entityList[0]
        
    def runLoop(self):
        while self.running :
            self.collectInputs()
            self.updateWorld()
            pygame.display.update()
            pygame.display.flip()
            self.display.fill((0,0,0))
            self.clock.tick(60)
        pygame.quit()

    def populateWord(self):
        self.entityList.append(entity.Entity(0,0,(0,255,0),50,50))
        self.entityList.append(entity.Entity(400,400,(255,0,0),50,50))
        self.entityList.append(crop.Crop(200,200))
        

    def updateWorld(self):
        for e in self.entityList:
            if isinstance(e,crop.Crop):
                e.grow()
                if e.isAlive == True:
                    e.draw(self.display)
                   # print("Update isAlive val == " + str(e.isAlive))
                else:
                    self.entityList.remove(e)
                    print("removed crop")
            
            else:
                if(e.isAlive == True):
                    e.draw(self.display)
                else:
                    self.entityList.remove(e)
            for o in self.entityList:
                if isinstance(e,entity.Entity) and isinstance(o,entity.Entity):
                    if e == o:
                        continue
                    else:
                        e.detect(o)
        self.player.draw(self.display)
    
    def checkForHarvest(self):
        for c in self.entityList:
            if(isinstance(c,crop.Crop)):
                self.player.harvest(c)

    def collectInputs(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                crashed = True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    self.player.x -=50
                    print("True")
                elif event.key == pygame.K_RIGHT:
                    self.player.x +=50
                elif event.key == pygame.K_DOWN:
                    self.player.y +=50
                elif event.key == pygame.K_UP:
                    self.player.y -= 50
                elif event.key == pygame.K_e:
                    self.checkForHarvest()
                elif event.key == pygame.K_q:
                    print("GAME CLOSING - DEBUG EXIT SWITCH ACTIVATED")
                    crashed = True
                    quit()

    

