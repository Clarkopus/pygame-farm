from utils import inventory
import pygame
class Entity:

    def __init__(self,x,y,color,width,height):
        self.x = y
        self.y = y
        self.color = color
        self.width = width
        self.height = height
        self.area = height * width
        self.isAlive = True
        self.inventory = inventory.Inventory()

    
    def draw(self,display):
        pygame.draw.rect(display,self.color,(self.x,self.y,self.width,self.height),1)

    def detect(self, other):
        x = self.x * self.area
        y = self.y * self.area

        otherX = other.x * other.area
        otherY = other.y * other.area

        if x == otherX and y == otherY:
            self.isAlive = False
            other.isAlive = False

    def harvest(self,crop):

        # Test change to check on area alone
        x = self.x * crop.area
        y = self.x * crop.area

        cropX = crop.x * crop.area
        cropY = crop.y * crop.area

        if(x == cropX and y == cropY):
            val = crop.getHarvest()
            self.inventory.addItem(val)
            print("Collected - " + val)
        else:
            print("failed")
            print(str(crop.x)+" " + str(crop.y))

        
