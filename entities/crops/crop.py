import pygame
import time
class Crop:

    def __init__(self,x,y):
        self.harvest = False
        self.count = 0
        self.spawned = int(round(time.time())* 1000)
        self.color = (165,42,42)
        self.x = x
        self.y = y
        self.width = 100
        self.height = 100
        self.area = self.height * self.width
        self.isAlive = True
        self.texture = pygame.image.load("resources/crop.png").convert()
    
    def grow(self):
        if(self.harvest == True):
            return
        if(self.count == 1):
            self.texture = pygame.image.load("resources/crop_grown1.png").convert()
        elif(self.count == 2):
            self.texture = pygame.image.load("resources/crop_grown2.png").convert()
        elif(self.count ==3):
            self.texture = pygame.image.load("resources/crop_grown3.png").convert()
            self.harvest = True

        millis = int(round(time.time() * 1000))
        if(millis > self.spawned + 10000):
            self.spawned = millis
            self.count +=1

    def draw(self,display):
        display.blit(self.texture,(self.x,self.y))

    def getHarvest(self):
        if(self.count == 3):
            self.isAlive = False
            print("getHarvest isAlive val == " + str(self.isAlive))
            return "berry"

        